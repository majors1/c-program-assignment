#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

/* Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

*/

struct Node* createNode(int number) {
    struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));
    if (newNode == NULL) {
        printf("Error: Unable to allocate memory for a new node\n");
        exit(1);
    }

    newNode->number = number;
    newNode->next = NULL;
    return newNode;
}

// Function to print all nodes in the list
void printList(struct Node *head) {
    struct Node *current = head;
    printf("[");
    while (current != NULL) {
        printf("%d", current->number);
        current = current->next;

        if(current != NULL){
            printf(", ");
        }
    }
    printf("]\n");
}

// Function to add a node at the end of the list
void append(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL) {
        current = current->next;
    }
    current->next = newNode;
}


// Function to add a node at the beginning of the list
void prepend(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}



// Function to delete a node by value
void deleteByValue(struct Node **head, int value) {
    struct Node *current = *head, *prev = NULL;

    if (current != NULL && current->number == value) {
        *head = current->next;
        free(current);
        return;
    }

    while (current != NULL && current->number != value) {
        prev = current;
        current = current->next;
    }

    if (current == NULL) {
        printf("Error: Value not found in the linked list\n");
        return;
    }

    prev->next = current->next;
    free(current);
}

// Function to delete a node by key
void deleteByKey(struct Node **head, int key) {
    struct Node *current = *head, *prev;

    if (current != NULL && current->number == key) {
        *head = current->next;
        free(current);
        return;
    }

    while (current != NULL && current->number != key) {
        prev = current;
        current = current->next;
    }

    if (current == NULL) {
        printf("Error: Key not found in the linked list\n");
        return;
    }

    prev->next = current->next;
    free(current);
}

// Function to insert a new node after a given key
void insertAfterKey(struct Node **head, int key, int num) {
    struct Node *newNode = createNode(num);
    struct Node *current = *head;
    while (current != NULL) {
        if (current->number == key) {
            newNode->next = current->next;
            current->next = newNode;
            return;
        }
        current = current->next;
    }
    printf("Error: Key not found in the linked list\n");
}

// Function to insert a new node after a given key
void insertAfterValue(struct Node **head, int value, int num) {
    struct Node *newNode = createNode(num);
    struct Node *current = *head;
    while (current != NULL) {
        if (current->number == value) {
            newNode->next = current->next;
            current->next = newNode;
            return;
             }
        current = current->next;
    }
    printf("Error: Value not found in the linked list\n");
}


int main()
{
    struct Node *head = NULL;
    int choice, data,key,value;

    while (1)
    {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. prepend\n");
        printf("4. Delete by key\n");
        printf("5. Delete by value\n");
        printf("6. Insert value after key\n");
        printf("7. Insert value after value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);
        switch (choice)
        {

        case 1:
            printList(head);
            break;
        case 2:
            printf("Enter data to append: ");
            scanf("%d",&data);
            append(&head,data);
            break;
        case 3:
            printf("Enter data to prepend: ");
            scanf("%d",&data);
            prepend(&head,data);
            break;
        case 4:
            printf("Enter data to delete: ");
            scanf("%d",&data);
            deleteByKey(&head,data);
            break;
        case 5:
            printf("Enter data to delete by its value: ");
            scanf("%d",&data);
            deleteByValue(&head,data);
            break;
        case 6:
            printf("Enter data to insert: ");
            scanf("%d\n",&data);
            printf("Enter the key before the value at the position you want the next value to be inserted: ");
            scanf("%d\n",&key);
            insertAfterKey(&head,key,data);
            break;
        case 7:
            printf("Enter data to insert after value: ");
            scanf("%d\n",&data);
            printf("Enter the value before the value at the position you want the next value to be inserted: ");
            scanf("%d\n",&value);
            insertAfterValue(&head,value,data);
            break;

         case 8:
                printf("Exiting program.\n");
                exit(0);
            default:
                printf("Invalid choice. Please try again.\n");

        }
    }



    return 0;
}



