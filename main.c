//MASAGAZI RUTH
//STUDENT NO.:2300711051
//REG NO. :23/U/11051/EVE

//This program finds the roots of a quadratic equation in the form of ax^2 + bx +c = 0
//and returns the values of the roots and identifies the type of roots whether real or complex
#include <stdio.h>
#include <math.h>
int main() {
    double a, b, c, D, root1, root2, realPart, imaginaryPart;
    printf("NAME:MASAGAZI RUTH");
    printf("\nSTUDENT NO.:2300711051");
    printf("\nREG NO. :23/U/11051/EVE\n");

//D is the discriminant
    printf("\nEnter coefficient a: ");
    scanf("%lf", &a);
    printf("Enter coefficient b: ");
    scanf("%lf", &b);
    printf("Enter coefficient c: ");
    scanf("%lf", &c);
//exception handling ;The program also checks if a is zero, in which case the equation is not quadratic
// and the program prints an error message.
if (a == 0) {
        printf("Error: a cannot be zero\n");

   }else {
        D = b * b - 4 * a * c;
//If D is positive i.e D > 0, the equation has two real and distinct roots,
        if (D > 0) {
            //Real and distinct roots are positive roots but different
            printf("Real and distinct roots\n");
            root1 = (-b + sqrt(D)) / (2 * a);
            root2 = (-b - sqrt(D)) / (2 * a);
            printf("Root1 = %.lf\n", root1);
            printf("Root2 = %.lf\n", root2);

//If D is zero, the equation has one real and repeated root
        } else if (D == 0) {
            //Real and equal roots positive and similar
            printf("Real and equal\n");
            root1 = -b / (2 * a);
            root2 = root1;
            printf("root1 = %.1f\n", root1);
            printf("root2 = %.1f\n", root2);
//If D is negative i.e D < 0, the roots are complex and are split into real and imaginary parts.
        } else {
            //Complex roots have an real and imaginary part
            //complex roots are in the form of a + bi and a - bi
            printf("Complex roots\n");
            realPart = -b / (2 * a);
            imaginaryPart = sqrt(-D) / (2 * a);
            printf("Root1 = %.lf + %.lf i\n", realPart, imaginaryPart);
            printf("Root2 = %.lf - %.lf i\n", realPart, imaginaryPart);
        }
    }
    return 0;
}
